## Note de mes avancées.


#### Dans un premier temps, j'ai commencé par le porjet du site. J'ai analysé les 4 fichiers excel que j'ai reçu qui contiennent chaqu'un la meme strucure :

* CODE_POSTAL
* COMMUNE
* ADRESSE
* TELEPHON
* Mail

Le fichier CEGIDD à une colonne en plus 'Site principal/ antenne/consultation avancée'

#### J'ai d'abord fait une base de données Sqlite3 avec 5 table:

1 pour chaque fichier (x4) + 1 qui les regroupe toutes pour leurs structure commune.Cette base de données est provisoire car j'aurais sûrement besoin de modifier des choses pour les besoins du site après avoir discuté avec Joanna et Mathieu, mais j'ai déjà une première base sur laquelle travailler et à proposer.
Cette base de données est le fichier nomé 'data.db' dans le dossier général. 

#### Ensuite, un des besoins de ce projet est la mise en place d'une cartographie:

J'ai créer un fichier 'adresses.xlsx'. Ce fichier se concentre sur les données concernant les adresses des adérants, que j'ai récupéré dans les 4 fichiers que Joanna m'a envoyés.Ce fichier dedier au adresse contient 3 colonnes :

* commune
* adresse
* adresse_geoloc

Avec ces données, j'utilise un outils qui convertit des adresses en 'longitudes' et 'latitudes'. Ces deux données permettent de poser un point précis sur une carte.

Si je géolocalise avec les données de la colones 'commune', l'outils que j'utilise trouve très facilement les longitudes et latitudes car il y a peu d'info à traiter, mais les points ne seront biensur pas placé aussi précisément sur la carte, que si on rentre les adresses exacte. Si je met juste un nom de ville, on peut placer un point mais quelque part dans la ville.

Si je fait de meme sur la colone 'adresse', qui contient les adresses présente dans les fichiers de base également, l'outils trouve à peine 1/3 des localisations, car la structure des adresses rentré à la mains par vos soins dans les fichiers de base, ne collent pas toujours avec ce que l'outils peut traité comme données. (dans la forme, comme s' il parlait chinois et vous francais) 

J'ai donc créer une colonne suplémentaire que j'ai nommé 'adresse_geoloc', ou je convertit chaques fois la forme des adresses pour les besoins de l'outil (Donc j'ai manuellement fait la traduction de francais à son chinois pour rester dans l'exemple). À noté que si vous souhaithez garder cette structure pour la suite, pour de potentielle future adhérents à inclure sur la map, je remarque que pour que l'outils trouve les coordonnees gps en fonction d'une adresse, la structure idéal selon moi est :

* rue, ville + code postale 

Exemple : 

* 58 Rue Montalembert, Clermont-Ferrand 63000 

Si vous rentrez les adresses avec cette structure dans la colonne 'adresse_geoloc' au fur et à mesure, vous faciliterez la tâche du futur développeur qui fera la mise à jour. Je tiens à préciser que vos données sont propres et je vous remercie pour cela (pour ce qui est des 4 fichiers envoyé par Joanna), à part quelques détails comme les adresses pour le cadre de la géoloc, tout à une base standardisé et c'est agréable à manipuler coté développement.

#### _____

Après traitement des données du fichier 'adresses.xlsx', un nouveau fichier 'geo_add.xlsx' est créé.Ce fichier contient les colonnes:

* adresse_geoloc (ma traduction retranscrite dans ce fichier)
* location (L'interprétation de ma traduction par l'outil de géocodage pour qu'il trouve les coordonnées)
* Lat (la latitude)
* Lon (la longitude)

Ce fichier sera le socle de la cartographie. 'geo_add.xlsx' est le fond, la face cachée de l'iceberg. La catro est la forme, la face émergée que l'on voit et qu'on manipule.

#### _____

Sur ce lien, vous pouvez voir en bas de page à quoi ressemble la première cartographie que j'ai fait. Vous pouvez zoomer et clicker sur les point pour voir les adresses.
* https://valliermaxime.gitlab.io/fed_act_sol/

Les 3 premieres grilles juste ce qui resulte du processus du code mais j'ai déployer cette page pour que vous puissiez vous faire un premier avis pour qu'on discute de la suite.

